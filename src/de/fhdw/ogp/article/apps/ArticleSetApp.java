package de.fhdw.ogp.article.apps;

import de.fhdw.ogp.article.controller.ArticleSetCtr;
import de.fhdw.ogp.article.utils.Console;

public class ArticleSetApp {
    public static void main(String[] args) {
        Console.printlnMessage("ArticleSet App starts");
        ArticleSetCtr.runMainDialogue();
        Console.printlnMessage("ArticleSet App terminates");
    }
}
