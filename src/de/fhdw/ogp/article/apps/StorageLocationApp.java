package de.fhdw.ogp.article.apps;

import de.fhdw.ogp.article.controller.StorageLocationCtl;
import de.fhdw.ogp.article.utils.Console;

public class StorageLocationApp {
    public static void main(String[] args) {
        Console.printlnMessage("StorageLocation App started");
        StorageLocationCtl.runMainDialogue();
        Console.printlnMessage("StorageLocation App terminated");
    }
}
