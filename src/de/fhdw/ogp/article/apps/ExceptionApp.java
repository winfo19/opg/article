package de.fhdw.ogp.article.apps;

import de.fhdw.ogp.article.model.Article;
import de.fhdw.ogp.article.model.ArticleIdOverflowException;

public class ExceptionApp {
    public static void main(String[] args) {
        try {
            int id = Article.LAST_ID + 100;

            //noinspection unused
            Article a = new Article(id, "Test", 5.5f, null);
        } catch (ArticleIdOverflowException e) {
            e.printStackTrace();
        }
    }
}
