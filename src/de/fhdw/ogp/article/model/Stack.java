package de.fhdw.ogp.article.model;

public class Stack<T> {
    private Node top;

    public Stack() {}

    public void push(T value) {
        Node next = top;
        top = new Node(value, next);
    }

    public T pop() {
        if (top == null) {
            return null;
        }
        T value = top.value;
        top = top.next;
        return value;
    }

    public boolean isEmpty() {
        return top == null;
    }

    private class Node {
        private T    value;
        private Node next;

        public Node(T value, Node next) {
            this.value = value;
            this.next = next;
        }

        public T getValue() {
            return value;
        }

        public void setValue(T value) {
            this.value = value;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }
}
