package de.fhdw.ogp.article.model;

public class ArticleIdOverflowException extends RuntimeException {
    public ArticleIdOverflowException() {
        super();
    }

    public ArticleIdOverflowException(String message) {
        super(message);
    }
}
