package de.fhdw.ogp.article.model;

public enum Function {
    ADMINISTRATION, IT, MARKETING, SALES
}
