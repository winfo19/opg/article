package de.fhdw.ogp.article.model;

public interface Product extends Nameable {
    float getSalePrice();

    void setSalePrice(float value);
}
