package de.fhdw.ogp.article.model;

public class LotteryTicket implements Product {
    private String name;
    private float  salePrice;

    public LotteryTicket(String name, float salePrice) {
        this.name = name;
        this.salePrice = salePrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(float salePrice) {
        this.salePrice = salePrice;
    }

    @Override
    public String toString() {
        return "LotteryTicket{" + "name='" + name + '\'' + ", salePrice=" + salePrice + '}';
    }
}
