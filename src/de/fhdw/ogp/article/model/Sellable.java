package de.fhdw.ogp.article.model;

public interface Sellable extends Nameable, Measurable {}
