package de.fhdw.ogp.article.model;

public interface Nameable {
    String getName();

    void setName(String value);
}
