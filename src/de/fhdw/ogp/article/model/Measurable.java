package de.fhdw.ogp.article.model;

public interface Measurable {
    Unit getUnit();

    void setUnit(Unit value);
}
