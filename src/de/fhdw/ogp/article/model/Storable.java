package de.fhdw.ogp.article.model;

public interface Storable extends Measurable {
    int getOnStock();

    void setOnStock(int value);
}
